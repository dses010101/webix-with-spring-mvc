# Webix with Spring MVC

Spring MVC and Webix integration.

Статья посвящена интеграции JavaScript фреймворка Webix и Spring MVC.


Начнём с небольшой предыстории.

Я начал заниматься разработкой web-приложений в 2013 году. Конечно, задолго до этого, ещё в 90-х я интересовался html и разрабатывал простенькие web-странички, но это никак не было связано с моей профессиональной деятельностью (в то время в моде были толстые клиенты или даже настольные приложения).

Вернёмся в 2013 год. Исходя из требований работодателя было принято решение разрабатывать систему в виде web-приложения, но не используя фрэймворки. Это означает, что мы можем использовать Java SE/EE на бэкэнде и html/css/JavaScript на фронтэнде. Весь обмен данными между фронтэндом и бэкэндом был построен на технологии WebSocket. Правда были утилиты основанные на сервлетах, а также использовался XMLHttpRequest.

В 2016 году я начал использовать JSF.

Сейчас вошли в моду JavaScript фреймворки, такие как React, Vue, Angular и Webix. Этот список можно продолжать очень долго и от всего этого разнообразия волосы встают дыбом. Проблема вот в чём: если у нас  Java SE/EE на бэкэнде и html/css/JavaScript на фронтэнде + WebSocket либо Servlet/XMLHttpRequest посередине, то данные между фронтэндом и бэкэндом передаются ну очень явно, если же у нас Servlet/JSF или Spring MVC, то мы можем отправлять целые web-страницы в браузер. К тому же весь обмен данными в JSF или Spring MVC весьма неявный.

Итак, когда я начал думать об использовании Webix в приложении построенном на Spring MVC и Thymeleaf, то мне поначалу стало грустно. Но потом я подумал: "постойте, вы можете накручивать что угодно, но в веб всё работает по стандартам". После этого я сел и написал небольшой примерчик.

Начал я с серверной части:

@Controller
@RequestMapping("/forWebix")
public class MainController
{
    @GetMapping(value = "/json", produces = "application/json")
    @ResponseBody
    public String json()
    {
        return
                "[\n" +
                "    {\"name\": \"Cherry\", \"country\": \"China\", \"year\": 2005},\n" +
                "    {\"name\": \"Chevrolet Impala\", \"country\": \"USA\", \"year\": 2006}\n" +
                "]";
    }
}

Имея этот контроллер уже можно ввести в адресную строку браузера url: "http://localhost:8080/forWebix/json/"
и получить следующий результат:

Теперь можно переходить к Webix:
<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" href="http://cdn.webix.com/edge/webix.css" type="text/css">
    <script src="http://cdn.webix.com/edge/webix.js" type="text/javascript"></script>
</head>

<body>

<script type="text/javascript" charset="utf-8">
    webix.ui
    ({
        rows:
            [
                {
                    view: "datatable",
                    id: "datatable",
                    autoConfig: true,
                    url: "forWebix/json/"
                }
            ]
    });
</script>

</body>

</html>

Введя в адресную строку браузера url: "http://localhost:8080/forWebix/json/"
получим следующее:

Исходники:
https://gitlab.com/dses010101/webix-with-spring-mvc.git

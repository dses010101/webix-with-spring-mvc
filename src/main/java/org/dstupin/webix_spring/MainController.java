package org.dstupin.webix_spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/forWebix")
public class MainController
{
    @GetMapping(value = "/json", produces = "application/json")
    @ResponseBody
    public String json()
    {
        return
                "[\n" +
                "    {\"name\": \"Cherry\", \"country\": \"China\", \"year\": 2005},\n" +
                "    {\"name\": \"Chevrolet Impala\", \"country\": \"USA\", \"year\": 2006}\n" +
                "]";
    }
}

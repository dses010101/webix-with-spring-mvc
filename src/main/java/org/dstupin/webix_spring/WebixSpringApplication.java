package org.dstupin.webix_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebixSpringApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(WebixSpringApplication.class, args);
    }
}
